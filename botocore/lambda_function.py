import boto3
import os
import logging
from mutagen.easyid3 import EasyID3
from mutagen.mp3 import MP3
from decimal import Decimal

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# AWS configuration:
s3 = boto3.client('s3')
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(os.environ['DYNAMO_TABLE'])


def object_handler(event, context):
    for record in event['Records']:
        bucket = record['s3']['bucket']['name']
        key = record['s3']['object']['key']

        object_name =  os.rename('/tmp/' + key, '/tmp/' + '1234.m4a')
        print(os.listdir('/tmp'))

        try:
            s3.Bucket(bucket).download_file(key, '/tmp/' + key)

        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                print("The object does not exist.")
            else:
                raise

        # Print helpful statistics from function context to logs:
        print("Mem. limits(MB):", context.memory_limit_in_mb)
        print("Function remaining(ms):", context.get_remaining_time_in_millis())
        # return bucket, key

        # Get EasyID3 tags from audio_object, then create dictionary data type:
        audio = EasyID3(object_name)
        audio_attributes = dict(audio.items())

        # Create object and get bitrate, audio file duration:
        built_audio_object = MP3(audio_object)
        decimal_length = Decimal(str(built_audio_object.info.length))

        # Add all metadata to DynamoDB:
        response = table.update_item(
            UpdateExpression="set ID3_tags = :i, bitrate = :b, decimal_length = :l",
            Key={
                'audio_object_name': audio_object,
            },
            ExpressionAttributeValues={
                ':i': audio_attributes,
                ':b': built_audio_object.info.bitrate,
                ':l': decimal_length,

            },
            ReturnValues="UPDATED_NEW"
        )

